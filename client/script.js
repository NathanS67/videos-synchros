const server_address = location.hostname;
const sock = ["127.0.0.1", "localhost"].includes(server_address) ? new WebSocket("ws://" + server_address + "/wg/") : new WebSocket("wss://" + server_address + "/wg/");
let is_paused = true;

const btn_play = document.getElementById("btn_play");
const btn_submit = document.getElementById("btn_submit");
const current_time = document.getElementById("current_time");
const desc = document.getElementById("desc");
const duration = document.getElementById("duration");
const player = document.getElementById("player");
const progress_bar = document.getElementById("progress_bar");
const title = document.getElementById("title");
const url = document.getElementById("url");
const volume = document.getElementById("volume");
const panelvid = document.getElementById("panelvid");

const PLAY = {
    type: "play",
    text: "Video is playing..."
};

const PAUSE = {
    type: "pause",
    text: "Video is paused..."
};

const play_pause = () => {
    if (is_paused) {
        sock.send(JSON.stringify(PLAY));
    } else {
        sock.send(JSON.stringify(PAUSE));
    }
};

const send_id = (url) => {
    let pos = url.indexOf("=") + 1;
    let data = {
        type: "video_id",
        id: url.substring(pos, url.length)
    };
    sock.send(JSON.stringify(data));
};

const update_progress_bar = (e) => {
    e.preventDefault();
    progress_bar.value = player.currentTime;
    current_time.innerHTML = player.currentTime;
};

const load_videos = () => {
    if (url.value.length > 0) {
        if (url.value.substring(0, 4) == "http") {
            send_id(url.value);
        } else {
            fetch("https://invidious.bretzel.ga/api/v1/search?q=" + url.value)
                .then((res) => {
                    if (res.ok) {
                        return res;
                    } else {
                        return res.text()
                            .then((text) => {
                                throw new Error(text);
                            })
                    }
                })
                .then((res) => {
                    return res.json();
                })
                .then((data) => {
                    panelvid.innerHTML = "";
                    data.forEach(element => {
                        panelvid.innerHTML += "<div id=vid onclick=\"send_id(\'https://invidious.bretzel.ga/watch?v=" + element.videoId + "\');\">" +
                            "<img id=\"vidthumb\" src=\"" + element.videoThumbnails[4].url + "\" alt=\"Miniature\"></img" +
                            "<p>" + element.title + "</p>" +
                            "</div>";
                    });
                })
                .catch((err) => {
                    console.log(err);
                })
        }
    }
}

const change_time = (e) => {
    e.preventDefault();
    let time = progress_bar.value;
    let data = {
        type: "seek",
        value: time
    };
    sock.send(JSON.stringify(data));
};

const handle_message = (data) => {
    switch (data.type) {
        case ("play"):
            player.play();
            btn_play.textContent = "Pause";
            is_paused = false;
            break;
        case ("pause"):
            player.pause();
            btn_play.textContent = "Play";
            is_paused = true;
            break;
        case ("video"):
            console.log(data);
            player.src = data.vidinfos[0];
            title.innerHTML = data.vidinfos[1];
            desc.innerHTML = data.vidinfos[2];
            progress_bar.max = data.vidinfos[3];
            duration.innerHTML = data.vidinfos[3];
            player.load();
            break;
        case ("seek"):
            player.currentTime = data.value;
            break;
    }
};

btn_submit.addEventListener("click", (e) => {
    e.preventDefault();
    load_videos();
});

const main = () => {

    sock.onopen = (event) => {
        console.log("ok");
    };

    sock.onmessage = (event) => {
        const data = JSON.parse(event.data);
        handle_message(data);
    };

    btn_play.addEventListener("click", (e) => {
        e.preventDefault();
        play_pause();
    });

    url.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            load_videos();
        }
    });

    volume.addEventListener("input", (e) => {
        e.preventDefault();
        player.volume = volume.value / 100;
    });

    player.addEventListener("timeupdate", update_progress_bar);

    progress_bar.addEventListener("input", change_time);
};

main();