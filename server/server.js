const Websocket = require('ws');
const fetch = require('node-fetch');
const wss = new Websocket.Server({ port: 9420, clientTracking: true });
let current_video = null;

const get_video = (vid_id) => {
    return fetch("https://invidious.bretzel.ga/api/v1/videos/" + vid_id)
        .then((res) => {
            if (res.ok) {
                return res;
            } else {
                return res.text()
                    .then((text) => {
                        throw new Error(text);
                    })
            }
        })
        .then((res) => {
            return res.json();
        })
        .catch((e) => {
            console.log(e);
        })
};

const handle_message = (msg) => {
    switch (msg.type) {
        case "play":
            console.log(msg.text);
            wss.clients.forEach((client) => {
                let response = {
                    type: "play"
                }
                client.send(JSON.stringify(response));
            });
            break;
        case "pause":
            console.log(msg.text);
            wss.clients.forEach((client) => {
                let response = {
                    type: "pause"
                }
                client.send(JSON.stringify(response));
            });
            break;
        case "video_id":
            get_video(msg.id)
                .then((data) => {
                    //let url = data.adaptiveFormats[0].url;//Format sans son
                    let url = data.formatStreams[0].url; //Format avec son
                    let title = data.title;
                    let desc = data.descriptionHtml;
                    let duration = data.lengthSeconds;
                    let response = {
                        type: "video",
                        vidinfos: [url, title, desc, duration]
                    };
                    console.log("Sending video...");
                    current_video = response;
                    wss.clients.forEach((client) => {
                        client.send(JSON.stringify(response));
                    });
                })
                .catch((err) => {
                    console.log(err);
                })
            break;
        case "seek":
            let time = msg.value;
            console.log("Seeking to " + time + "s...");
            let response = {
                type: "seek",
                value: time
            };
            wss.clients.forEach((client) => {
                client.send(JSON.stringify(response));
            });
            break;
    }
};

const main = () => {
    console.log("server started");

    wss.on('connection', (ws) => {
        if (current_video !== null) {
            ws.send(JSON.stringify(current_video));
        }

        console.log("client connected");

        ws.on('message', (message) => {
            let msg = JSON.parse(message);
            handle_message(msg);

        });
    });

    wss.on('error', (err) => {
        console.log(err);
    });

};

main();